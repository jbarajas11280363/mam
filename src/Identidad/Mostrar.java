package Identidad;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
public class Mostrar
{
    public void Mostrardatos(Orden orden,Producto producto,Lineaorden lineaorden)
    {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("MuchosaMuchosPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Collection<Orden> lista_orden;
        lista_orden = em.createNamedQuery("Orden.findAll").getResultList();
        System.out.println("La orden");
        for (Orden el : lista_orden)
        {
            System.out.println("ID de la orden: "+el.getIdOrden()+"\nFecha de la orden: "+el.getFechaOrden());
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++");
        
        }        
        System.out.println("El producto");
        Collection<Producto> lista_producto;
        lista_producto = em.createNamedQuery("Producto.findAll").getResultList();

        for (Producto el : lista_producto)
        {
            System.out.println("ID del producto: "+el.getIdProducto()+"\nDescripcion del producto: "+el.getDescripcion()+"\nPrecio del producto: "+el.getPrecio());
            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        System.out.println("Linea de la orden");
        Collection<Lineaorden> lista_lineaorden;
        lista_lineaorden = em.createNamedQuery("Lineaorden.findAll").getResultList();

        for (Lineaorden el : lista_lineaorden)
        {
            System.out.println("ID de orden: "+el.getOrden().getIdOrden()+"\nID del producto: "+el.getProducto().getIdProducto()+"\nCantidad: "+el.getCantidad());
            System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
        }
        
    }
}

































/*
        System.out.println("------------Datos Orden-------------");
        System.out.println("ID orden: "+orden.getIdOrden());
        System.out.println("ID orden: "+orden.getFechaOrden());
        System.out.println("------------Datos Producto----------");
        System.out.println("ID orden: "+producto.getIdProducto());
        System.out.println("ID orden: "+producto.getDescripcion());
        System.out.println("ID orden: "+producto.getPrecio());
        System.out.println("------------Datos Lineaorden--------");
        System.out.println("ID orden: "+lineaorden.getLineaordenPK().getIdOrden());
        System.out.println("ID orden: "+lineaorden.getLineaordenPK().getIdProducto());
        System.out.println("ID orden: "+lineaorden.getCantidad());
        */